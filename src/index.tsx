import App from "./App";
import { StrictMode } from "react";
import reportWebVitals from "./reportWebVitals";
import * as ReactDOMClient from "react-dom/client";
import "./index.scss";

const container = document.getElementById("root");

const root = ReactDOMClient.createRoot(container as HTMLElement);

root.render(
  <StrictMode>
    <App />
  </StrictMode>
);

reportWebVitals();
