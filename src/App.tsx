import React, { Suspense } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
const Contacts = React.lazy(() => import("./containers/contacts"));

function App() {
  return (
    <BrowserRouter>
      <Suspense fallback={<p> Loading...</p>}>
        <Routes>
          <Route path="/*" element={<Contacts />} />
        </Routes>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
