import { useState } from "react";
import MobileItem from "../../../components/mobileItem";
import Modal from "../../../components/modal";
import ContactsCard from "../card";
import { ContactProps, ContactType } from "../interface";
import styles from "./contactsMobile.module.scss";

interface SelectedContactType {
  visible: boolean;
  data?: ContactProps;
}

const ContactsMobile = ({ contacts }: { contacts: ContactType }) => {
  const [selectedContact, setSelectedContact] = useState<SelectedContactType>({
    visible: false,
    data: undefined,
  });

  const closeModal = () =>
    setSelectedContact({ visible: false, data: undefined });

  return (
    <div className={styles["contactsMobile"]}>
      {Object.keys(contacts)
        .sort((a, b) => (a > b ? 1 : a < b ? -1 : 0))
        .map((contact, index) => (
          <MobileItem
            key={index}
            items={contacts[contact]}
            groupedBy={contact}
            openModal={(data) => setSelectedContact({ visible: true, data })}
          />
        ))}
      <Modal visible={selectedContact.visible} onClose={closeModal}>
        {selectedContact.data && (
          <div className={styles["contactsMobile__detail"]}>
            <ContactsCard
              contact={selectedContact.data!}
              closeCard={closeModal}
            />
          </div>
        )}
      </Modal>
    </div>
  );
};

export default ContactsMobile;
