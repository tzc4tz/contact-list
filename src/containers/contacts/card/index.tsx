import { MouseEvent, useMemo } from "react";
import { ContactProps } from "../interface";
import styles from "./contactsCard.module.scss";

interface Props {
  contact: ContactProps;
  closeCard: (e: MouseEvent<HTMLDivElement>) => void;
}

const ContactsCard = ({ contact, closeCard }: Props) => {
  const card = useMemo(() => {
    const { name, picture, email, phone, location, login } = contact;
    return (
      <div
        className={styles["contactCard"]}
        onClick={(e) => e.stopPropagation()}
      >
        <div className={styles["contactCard--close"]} onClick={closeCard} />
        <div className={styles["contactCard--avatar"]}>
          <img src={picture.medium} loading="lazy" alt="avatar" />
        </div>
        <div className={styles["contactCard__info"]}>
          <div className={styles["contactCard__info--name"]}>
            {name.first}, {name.last}
          </div>
          <div className={styles["contactCard__info__rows"]}>
            <span>e-mail</span>
            <span>{email}</span>
          </div>
          <div className={styles["contactCard__info__rows"]}>
            <span>phone</span>
            <span>{phone}</span>
          </div>
          <div className={styles["contactCard__info__rows"]}>
            <span>street</span>
            <span>
              {location.street.number} {location.street.name}
            </span>
          </div>
          <div className={styles["contactCard__info__rows"]}>
            <span>city</span>
            <span>{location.city}</span>
          </div>
          <div className={styles["contactCard__info__rows"]}>
            <span>state</span>
            <span>{location.state}</span>
          </div>
          <div className={styles["contactCard__info__rows"]}>
            <span>postcode</span>
            <span>{location.postcode}</span>
          </div>
        </div>
        <div className={styles["contactCard__info--username"]}>
          USERNAME {login.username}
        </div>
      </div>
    );
  }, [contact]);

  return card;
};

export default ContactsCard;
