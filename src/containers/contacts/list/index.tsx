import styles from "./contactsList.module.scss";
import ListItem from "../../../components/listItem";
import { ContactProps, ContactType } from "../interface";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";

const ContactsList = ({ contacts }: { contacts: ContactType }) => {
  const params = useParams();
  const currentContacts = contacts?.[params?.["*"] as keyof ContactType];
  const [selectedContact, setSelectedContact] = useState<ContactProps>();

  useEffect(() => {
    setSelectedContact(undefined);
  }, [params]);

  return (
    <div className={styles["contactsList"]}>
      {currentContacts?.map((contact, index) => (
        <ListItem
          key={index}
          contact={contact}
          selectContact={(item) => setSelectedContact(item)}
          isSelected={selectedContact?.login?.uuid === contact?.login?.uuid}
        />
      ))}
    </div>
  );
};

export default ContactsList;
