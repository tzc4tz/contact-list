import styles from "./contacts.module.scss";
import { useEffect, useState } from "react";
import ContactsHeader from "./header";
import { ContactProps, ContactType } from "./interface";
import ContactsList from "./list";
import ContactsMobile from "./mobile";

const Contacts = () => {
  const [contacts, setContacts] = useState<ContactType>({});

  useEffect(() => {
    fetch("https://randomuser.me/api/?results=100")
      .then((res) => res.json())
      .then((json) => {
        const response = json.results;
        const english = /^[A-Za-z]*$/;
        const reducedContacts: ContactType = response.reduce(
          (contact: ContactType, item: ContactProps) => {
            const name = item.name.first.charAt(0);
            if (!english.test(item.name.first)) {
              if (contact["*"] == null) contact["*"] = [];
              contact["*"].push(item);
            } else {
              if (contact[name] == null) contact[name] = [];
              contact[name].push(item);
            }
            return contact;
          },
          {}
        );
        setContacts(reducedContacts);
      });
  }, []);

  return (
    <div className={styles["contacts"]}>
      <span className={styles["contacts--title"]}>Contact List</span>
      <ContactsHeader contacts={contacts} />
      <ContactsList contacts={contacts} />
      <ContactsMobile contacts={contacts} />
    </div>
  );
};

export default Contacts;
