import SortItem from "../../../components/sortItem";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { ContactType } from "../interface";
import styles from "./contactsHeader.module.scss";

const ContactsHeader = ({ contacts }: { contacts: ContactType }) => {
  const params = useParams();
  const navigate = useNavigate();
  const [activeTab, setActiveTab] = useState("");
  const alpha = Array.from(Array(27)).map((_, i) => (i === 26 ? 0 : i + 65));
  const alphabet = alpha.map((x) => (!x ? "*" : String.fromCharCode(x)));

  useEffect(() => {
    if (params["*"]) setActiveTab(params["*"]);
  }, [params]);

  return (
    <div className={styles["header"]}>
      {alphabet.map((char) => (
        <SortItem
          key={char}
          character={char}
          quantity={contacts?.[char]?.length}
          onSort={() => navigate(`/${char}`)}
          isActive={activeTab === char}
        />
      ))}
    </div>
  );
};

export default ContactsHeader;
