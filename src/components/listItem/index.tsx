import ContactsCard from "../../containers/contacts/card";
import { ContactProps } from "../../containers/contacts/interface";
import styles from "./listItem.module.scss";

interface Props {
  contact: ContactProps;
  selectContact: (item?: ContactProps) => void;
  isSelected: boolean;
}

const ListItem = ({ contact, selectContact, isSelected }: Props) => {
  const { name } = contact;
  return (
    <div className={styles["listItem"]} onClick={() => selectContact(contact)}>
      {name.first} {name.last}
      {isSelected && (
        <ContactsCard
          contact={contact}
          closeCard={(e) => {
            e.stopPropagation();
            selectContact(undefined);
          }}
        />
      )}
    </div>
  );
};

export default ListItem;
