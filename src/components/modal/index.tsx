import { ReactNode, useEffect, useRef, useState } from "react";
import ReactDOM from "react-dom";
import styles from "./modal.module.scss";

interface Props {
  children: ReactNode;
  visible: boolean;
  onClose: () => void;
}

const Modal = ({ children, visible, onClose }: Props) => {
  const [mounted, setMount] = useState(false);
  const ref = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    ref.current = document.querySelector("#portal");
    if (visible) {
      document.documentElement.classList.add(styles["stop-scrolling"]);
    } else {
      document.documentElement.classList.remove(styles["stop-scrolling"]);
    }
  }, [visible]);

  useEffect(() => {
    setMount(true);
    return () => {
      setMount(false);
    };
  }, []);

  return mounted && ref.current && visible
    ? ReactDOM.createPortal(
        <div className={styles["modal__container"]}>
          {children}
          <div onClick={onClose} className={styles["modal__mask"]} />
        </div>,
        ref.current
      )
    : null;
};

export default Modal;
