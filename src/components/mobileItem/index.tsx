import { ContactProps } from "../../containers/contacts/interface";
import styles from "./mobileItem.module.scss";

interface Props {
  items: ContactProps[];
  groupedBy: string;
  openModal: (contact: ContactProps) => void;
}

const MobileItem = ({ items, groupedBy, openModal }: Props) => {
  return (
    <div className={styles["mobileItem"]}>
      <div className={styles["mobileItem--group"]}>
        <span>{groupedBy}</span>
      </div>
      <div className={styles["mobileItem__contacts"]}>
        {items?.map((item, index) => (
          <div key={index} onClick={() => openModal(item)}>
            {item.name.first} {item.name.last}
          </div>
        ))}
      </div>
    </div>
  );
};

export default MobileItem;
