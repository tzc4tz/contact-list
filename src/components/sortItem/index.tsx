import styles from "./sortItem.module.scss";
import cn from "classnames";

interface Props {
  character: string;
  quantity: number;
  onSort: () => void;
  isActive: boolean;
}

const SortItem = ({ character, quantity, onSort, isActive }: Props) => {
  return (
    <div
      className={cn(
        styles["sortItem"],
        !quantity ? styles["sortItem--disabled"] : "",
        isActive ? styles["sortItem--active"] : ""
      )}
      onClick={onSort}
    >
      {character.toLowerCase()}
      <span className={styles["sortItem--quantity"]}>{quantity ?? 0}</span>
    </div>
  );
};

export default SortItem;
